# k8s_exercise



## Getting started

This project builds and deploy a simple application into EKS cluster. All the kubernetes manifests are packaged into a helm chart(declarative approach) and chart will be deployed into the cluster.

## CI

The project has CI that builds a docker image and pushes it to ECR. It then deploys the helm chart which is based on the image that is built. 
It has three stages. 

#### build stage

Builds a docker image from Dockerfile using kaniko build tool instead of DIND and uploads it to ECR public repository.

#### deploy stage

Deploys the helm chart tagging the image that is built in previous stage into EKS cluster(2 worker nodes)

#### test stage

It runs a curl command to test the connectivity with application that is exposed using NodePort Service in the cluster.


### Additionals

`HPA` has been configured to scale the pods up and down in response to changes in workload.

`PDB` has been configured to ensure application can resist without downtime to host issues, and maintenance operation

`podAntiAffinity` has been set for the deployment to ensure application pods are distributed across the cluster nodes

`PodSecurityPolicy(PSP)` has been set for the deployment to ensure application runs in non-privileged(non-root user) mode on the container.

`Role` and `Rolebinding` has been created to disable exec into pod containers. Not working entirely. 

### Alternatives

Terraform provider `kubernetes` and Terraform resources like `kubernetes_deployment_v1`, `kubernetes_service` can also be used for deploying application into k8s cluster as an alternative to Helm chart but Helm as a package manager would make it easy to manage manifests in projects with multiple/numerous service deployments.
