FROM nginx:1.14.2
COPY site-content/index.html /usr/share/nginx/html
RUN useradd --uid 10000 runner && \
         chown runner /usr/share/nginx/html && \
         chown runner /usr/share/nginx 